﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace welcome_to_the_dungeon.BL.GUIManager
{
    static class GUIMethods
    {
        /// <summary>
        /// Adds rows to chosen grid
        /// </summary>
        public static Grid AddingColumns_ToGrid(this Grid grid, int limit = 1)
        {
            for (int i = 0; i < limit; i++)
            {
                grid.ColumnDefinitions.Add(new ColumnDefinition());
            }
            return grid;
        }

        /// <summary>
        /// Add control to chosen grid
        /// </summary>
        public static Grid AddingControl(this Grid grid, Control cl, string name, string number, int row, int column)
        {
            if (name != "")
                cl.Name = name + number;
            grid.Children.Add(cl);
            cl.SetValue(Grid.RowProperty, row);
            cl.SetValue(Grid.ColumnProperty, column);
            return grid;
        }

    }
}
