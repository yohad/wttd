﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using welcome_to_the_dungeon.BL;

namespace welcome_to_the_dungeon.BL
{
    class UIStateManager : ObservableObject
    {
        private BitmapImage _HerosImageSource;
        private BitmapImage _MonsterImageSource;

        public BitmapImage HerosImageSource
        {
            get { return _HerosImageSource; }
            set
            {
                _HerosImageSource = value;
                RaisePropertyChangedEvent("HerosImageSource");
            }
        }

        public BitmapImage MonsterImageSource
        {
            get { return _MonsterImageSource; }
            set
            {
                _MonsterImageSource = value;
                RaisePropertyChangedEvent("MonsterImageSource");
            }
        }
    }
}
