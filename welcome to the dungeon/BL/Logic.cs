﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static welcome_to_the_dungeon.Stage;

namespace welcome_to_the_dungeon.BL
{
    class Logic
    {
        public int turn { get; set; }
        public List<Monster> listofmonstercard { get; set; }

        public Logic()
        {
            listofmonstercard = Monsters.MonsterList;
        }

        public int drawcard(List<Monster> ListMonsters)
        {
            Random rnd = new Random();
            int monsterindex = -1;
            while (ListMonsters.Count!=0)
            {
                monsterindex = rnd.Next(0, ListMonsters.Count);
                if (ListMonsters[monsterindex] != null)
                    break;
            }

            return monsterindex;
        }
        
         
    }
}
