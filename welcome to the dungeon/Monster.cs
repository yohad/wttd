﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using welcome_to_the_dungeon.Global;

namespace welcome_to_the_dungeon
{
    public class Monster
    {
        #region Prop
        public int strength { get; set; }
        public BitmapImage imagesource { get; set; }
        #endregion

        #region CTOR
        public Monster(int _strngth, string _imagesource)
        {
            strength = _strngth;
            imagesource = _imagesource.ConvertStringtoImageSource();
            //Thread.Sleep(10000);
            //string directoryPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
            //imagesource = Path.Combine(directoryPath, _imagesource).ConvertStringtoImageSource();
        } 
        #endregion
    }
}
