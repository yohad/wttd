﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace welcome_to_the_dungeon.Global
{
    public static class MyExtensionMethods
    {
        public static BitmapImage ConvertStringtoImageSource(this string path, UriKind urikind = UriKind.Absolute)
        {
            Uri imageUri = new Uri(path, urikind);
            BitmapImage bi = new BitmapImage(imageUri);
            //Thread.Sleep(5000);
            return bi;
        }

        public static string ArrayToString(this object[] array)
        {
            string str = "";
            foreach (object obj in array)
            {
                if ((int)obj == 0)
                    str = "chosen monster";
                else
                    str = str + " " + obj;
            }
            return str;
        }

        #region GUIMethods
        /// <summary>
        /// Adds rows to chosen grid
        /// </summary>
        public static Grid AddingColumns_ToGrid(this Grid grid, int limit = 1)
        {
            for (int i = 0; i < limit; i++)
            {
                grid.ColumnDefinitions.Add(new ColumnDefinition());
            }
            return grid;
        }

        /// <summary>
        /// Add control to chosen grid
        /// </summary>
        public static Grid AddingControl(this Grid grid, UIElement UE, int row, int column)
        {
            grid.Children.Add(UE);
            UE.SetValue(Grid.RowProperty, row);
            UE.SetValue(Grid.ColumnProperty, column);
            return grid;
        }
        #endregion


    }
}
