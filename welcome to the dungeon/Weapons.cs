﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace welcome_to_the_dungeon
{
    static class Weapons
    {
        #region WarriorWeapons
        public static List<Weapon> WarriorWeapons = new List<Weapon>
        {
            new Weapon("Torch",@"Assets\Images\Torch.jpg",new int[] {1,2,3 } ),
            new Weapon("Armor",@"Assets\Images\Armor.jpg",5 ),
            new Weapon("Shield",@"Assets\Images\Shield.jpg",3 ),
            new Weapon("Holy Grail",@"Assets\Images\Holy Grail.jpg",new int[] {2,4,6 } ),
            new Weapon("Dragon Spear",@"Assets\Images\Dragon Spear.jpg",new int[] {9} ),
            new Weapon("Vorpal Sword",@"Assets\Images\Vorpal Sword.jpg",new int[] {0} ) // 0 stand for a chosen monster
        }; 
        #endregion

        public static List<Weapon> MageWeapons = new List<Weapon>();
        public static List<Weapon> RogueWeapons = new List<Weapon>();
        public static List<Weapon> BarbarianWeapons = new List<Weapon>();


    }
}
