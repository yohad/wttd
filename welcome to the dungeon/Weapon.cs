﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using welcome_to_the_dungeon.Global;

namespace welcome_to_the_dungeon
{
    public class Weapon
    {
        #region Prop
        public string Name { get; set; }
        public BitmapImage ImageSource { get; set; }
        public object[] Killing { get; set; }
        public int Shield { get; set; }
        public bool Isweapon { get; set; }
        #endregion

        #region Ctor

        /// <summary>
        /// For Weapons
        /// </summary>
        public Weapon(string _name,string _ImageSource, object[] _killing)
        {
            Name = _name;
            string directoryPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
            ImageSource = Path.Combine(directoryPath, _ImageSource).ConvertStringtoImageSource();
            Killing = _killing;
            Isweapon = true;
        } 

        /// <summary>
        /// For armors tools
        /// </summary>
        public Weapon(string _name, string _ImageSource, int _Shield) 
        {
            Name = _name;
            string directoryPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
            ImageSource = Path.Combine(directoryPath, _ImageSource).ConvertStringtoImageSource();
            Shield = _Shield;
            Isweapon = false;
        }
        #endregion
    }
}
