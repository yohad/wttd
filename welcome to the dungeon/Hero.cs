﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using welcome_to_the_dungeon.Global;

namespace welcome_to_the_dungeon
{
    public class Hero
    {
        #region Prop
        public string Name { get; set; }
        public BitmapImage ImageSource { get; set; }
        public List<Weapon> HerosWeapon { get; set; }
        public int Life { get; set; }
        #endregion

        #region CTOR
        public Hero(string _name, string _imagesource, List<Weapon> _Weapons, int _Life)
        {
            Name = _name;
            ImageSource = _imagesource.ConvertStringtoImageSource(UriKind.Relative);
            HerosWeapon = _Weapons;
            Life = _Life;
        }
        #endregion

        #region Functions
        public string HeroesSkills()
        {
            string heroesskill = "";

            foreach (Weapon wp in HerosWeapon)
            {
                if (wp.Isweapon)
                    heroesskill = heroesskill + wp.Name + " kill: " + wp.Killing.ArrayToString() + " \n";
                else
                    heroesskill = heroesskill + wp.Name + ": " + wp.Shield + " HP \n";
            }

            return heroesskill;
        } 
        #endregion

    }
}
