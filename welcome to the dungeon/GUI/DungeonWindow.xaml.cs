﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using welcome_to_the_dungeon.Global;
using welcome_to_the_dungeon.BL;
using System.Threading;

namespace welcome_to_the_dungeon.GUI
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class DungeonWindow : Window
    {
        public Hero hero { get; set; }
        public bool FirstPlayer { get; set; }
        public List<Monster> MonstersInTheDungeon { get; set; }
        public List<Monster> MonstersInThePack { get; set; }
        public BitmapImage Monster_Image { get; set; }
        int monsterindex;
        Monster new_monster;
        UIStateManager usm = new UIStateManager();
        Logic gameslogic;
        //Monsters mon = new Monsters();

        public DungeonWindow(int heronumber)
        {
            InitializeComponent();
            this.DataContext = usm;
            gameslogic = new Logic();
            MonstersInThePack = Stage.Monsters.MonsterList;
            hero = Stage.Heroes.HeroesList[heronumber];
            CreateWeapons();
        }

        private void DraftButton_Click(object sender, RoutedEventArgs e)
        {
            monsterindex = gameslogic.drawcard(MonstersInThePack);
            new_monster = MonstersInThePack[monsterindex];
            MonstersInThePack.RemoveAt(monsterindex);
            MonsterImage.Source = new_monster.imagesource;
            MonsterStrength.Text = new_monster.strength.ToString();
        }

        private void PassButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void CreateWeapons()
        {
            WeaponsGrid.AddingColumns_ToGrid(hero.HerosWeapon.Count);
            for (int i = 0; i < hero.HerosWeapon.Count; i++)
            {
                Image WeaponImage = new Image();
                WeaponImage.MouseLeftButtonUp += WeaponImage_MouseLeftButtonUp;
                WeaponImage.Source = hero.HerosWeapon[i].ImageSource;
                WeaponImage.Height = 200;
                WeaponImage.Width = hero.HerosWeapon[i].ImageSource.Width / 10;
                WeaponImage.Name = "weapon" + i;
                //WeaponImage.Stretch = Stretch.Fill;
                WeaponImage.HorizontalAlignment = HorizontalAlignment.Center;
                WeaponImage.VerticalAlignment = VerticalAlignment.Center;
                WeaponImage.Visibility = Visibility.Visible;
                WeaponsGrid.AddingControl(WeaponImage, 0, i);
                TextBlock WeaponName = new TextBlock();
                WeaponName.Text = hero.HerosWeapon[i].Name;
                WeaponsGrid.AddingControl(WeaponName, 1, i);
                TextBlock WeaponAbility = new TextBlock();
                if (hero.HerosWeapon[i].Isweapon)
                {
                    WeaponAbility.Text = "kill " + hero.HerosWeapon[i].Killing.ArrayToString();
                }
                else
                {
                    WeaponAbility.Text = "+ " + hero.HerosWeapon[i].Shield + " HP";
                }
                WeaponsGrid.AddingControl(WeaponAbility, 2, i);
            }
        }

        private void WeaponImage_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {

        }

        private void MonsterImage_MouseEnter(object sender, MouseEventArgs e)
        {
            MessageBox.Show("hi");
        }

        private void MonsterImage_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            MessageBox.Show("hi");
        }
    }
}
