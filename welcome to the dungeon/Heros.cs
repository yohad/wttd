﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace welcome_to_the_dungeon
{
    static class Heros
    {
        public static List<Hero> HerosList = new List<Hero>()
        {
            new Hero("Warrior",@"Assets\images\warrior.jpg",Weapons.WarriorWeapons,3),
            new Hero("Mage",@"Assets\Images\mage.jpg",Weapons.MageWeapons,2),
            new Hero("Barbarian",@"Assets\Images\barbarian.jpg",Weapons.BarbarianWeapons,4),
            new Hero("Rogue",@"Assets\Images\rogue.jpg",Weapons.RogueWeapons,3),
        };
    }
}
