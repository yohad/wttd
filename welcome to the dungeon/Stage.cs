﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace welcome_to_the_dungeon
{
    static class Stage
    {
        public static class Heroes
        {
            public static List<Hero> HeroesList = new List<Hero>()
        {
            new Hero("Warrior",@"Assets\images\Heroes\warrior.jpg",Weapons.WarriorWeapons,3),
            new Hero("Mage",@"Assets\Images\Heroes\mage.jpg",Weapons.MageWeapons,2),
            new Hero("Barbarian",@"Assets\Images\Heroes\barbarian.jpg",Weapons.BarbarianWeapons,4),
            new Hero("Rogue",@"Assets\Images\Heroes\rogue.jpg",Weapons.RogueWeapons,3),
        };
        }

        public static class Weapons
        {
            #region WarriorWeapons
            public static List<Weapon> WarriorWeapons = new List<Weapon>
        {
            new Weapon("Torch",@"Assets\Images\Weapons\Torch.jpg",new object[] {1,2,3 } ),
            new Weapon("Armor",@"Assets\Images\Weapons\Armor.jpg",5 ),
            new Weapon("Shield",@"Assets\Images\Weapons\Shield.jpg",3 ),
            new Weapon("Holy Grail",@"Assets\Images\Weapons\Holy Grail.jpg",new object[] {2,4,6 } ),
            new Weapon("Dragon Spear",@"Assets\Images\Weapons\Dragon Spear.jpg",new object[] {9} ),
            new Weapon("Vorpal Sword",@"Assets\Images\Weapons\Vorpal Sword.jpg",new object[] {0} ) // 0 stand for a chosen monster
        };
            #endregion

            public static List<Weapon> MageWeapons = new List<Weapon>();
            public static List<Weapon> RogueWeapons = new List<Weapon>();
            public static List<Weapon> BarbarianWeapons = new List<Weapon>();


        }

        public static class Monsters
        {
            public static List<Monster> MonsterList = new List<Monster>()
        {
            new Monster(1,@"Assets\Images\Monsters\Goblin.jpg"),
            new Monster(2,@"Assets\Images\Monsters\Skeleton.jpg"),
            new Monster(2,@"Assets\Images\Monsters\Skeleton.jpg"),
            new Monster(3,@"Assets\Images\Monsters\Goblin.jpg"),
            new Monster(3,@"Assets\Images\Monsters\Goblin.jpg"),
            new Monster(4,@"Assets\Images\Monsters\Vampire.jpg"),
            new Monster(4,@"Assets\Images\Monsters\Vampire.jpg"),
            new Monster(5,@"Assets\Images\Monsters\Werewolf.jpg"),
            new Monster(5,@"Assets\Images\Monsters\Werewolf.jpg"),
            new Monster(6,@"Assets\Images\Monsters\Devil.jpg"),
            new Monster(7,@"Assets\Images\Monsters\Grim.jpg"),
            new Monster(9,@"Assets\Images\Monsters\Dragon.jpg"),
        };

            //byte[] monsters_type = new byte[] { 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 7, 9 };
            //byte[] monsters_game = new byte[] { 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 7, 9 };
            //byte[] monsters_in_dungeon = new byte[13];

            //public byte[] MonstersOnStart()
            //{
            //    for (int i = 0; i < 13; i++)
            //    {
            //        monsters_game[i] = monsters_type[i];
            //    }
            //    return monsters_game;
            //}
        }
    }

  
}
