﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace welcome_to_the_dungeon
{
    class Monsters
    { 
        byte[] monsters_type = new byte[] { 1,1,2,2,3,3,4,4,5,5,6,7,9 };
        byte[] monsters_game = new byte[] { 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 7, 9 };
        byte[] monsters_in_dungeon = new byte[13];

        public byte[] MonstersOnStart()
        {
            for(int i=0;i<13;i++)
            {
                monsters_game[i] = monsters_type[i];
            }
            return monsters_game;
        }
    }
}
