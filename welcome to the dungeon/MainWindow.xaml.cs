﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using welcome_to_the_dungeon.GUI;
using welcome_to_the_dungeon.BL;
using welcome_to_the_dungeon.Global;

namespace welcome_to_the_dungeon
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Prop
        UIStateManager USM = new UIStateManager();
        int Heronumber = 0;
        #endregion

        #region CTOR
        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = USM;
            ChangeHero(Heronumber);
        } 
        #endregion

        #region ArrowsEvents
        private void LeftArrowButton_Click(object sender, RoutedEventArgs e)
        {
            if (Heronumber == 0)
                Heronumber = Stage.Heroes.HeroesList.Count - 1;
            else
                Heronumber--;
            ChangeHero(Heronumber);
        }

        private void RightArrowButton_Click(object sender, RoutedEventArgs e)
        {
            if (Heronumber == Stage.Heroes.HeroesList.Count - 1)
                Heronumber = 0;
            else
                Heronumber++;
            ChangeHero(Heronumber);
        }
        #endregion

        #region EnterToTheDungeon
        private void Enter_Click(object sender, RoutedEventArgs e)
        {
            DungeonWindow dungeon_window = new DungeonWindow(Heronumber);
            dungeon_window.Show();
            Close();
        }
        #endregion

        #region Functions
        /// <summary>
        /// Changes the GUI after changing hero
        /// </summary>
        private void ChangeHero(int numberhero)
        {
            USM.HerosImageSource = Stage.Heroes.HeroesList[numberhero].ImageSource;
            HeroName.Text = Stage.Heroes.HeroesList[numberhero].Name;
            SkillsTextBlock.Text = "Life: " + Stage.Heroes.HeroesList[numberhero].Life + "\n"
                + Stage.Heroes.HeroesList[numberhero].HeroesSkills();
        } 
        #endregion
    }
}
